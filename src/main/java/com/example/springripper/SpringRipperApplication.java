package com.example.springripper;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Description;

@SpringBootApplication
public class SpringRipperApplication {

	@Bean
	@Description("commandLineRunner")
	public CommandLineRunner profilingControllerTest(ApplicationContext applicationContext) {
		return args -> {
			System.out.println(applicationContext);
			int i = 0;
		};
	}


	public static void main(String[] args) {
		SpringApplication.run(SpringRipperApplication.class, args);
	}

}

