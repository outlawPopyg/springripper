package com.example.springripper.parts.second;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SomeIntImpl implements SomeInt {

    @Override
    public void someMethod() {
        System.out.println("post construct in implemented class");
    }

    @Autowired
    public void anotherMethod(SomeComponent someComponent) {
        System.out.println(someComponent);
    }
}
