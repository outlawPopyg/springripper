package com.example.springripper.parts.second;

import javax.annotation.PostConstruct;

public interface SomeInt {
    @PostConstruct
    void someMethod();
}
