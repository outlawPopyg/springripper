package com.example.springripper.parts.second;

import com.example.springripper.parts.second.annotations.FirstQualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Configuration
public class FirstConfig {
    @Bean
    @FirstQualifier
    public String str1() { return "Groovy"; }

    @Bean
    @FirstQualifier
    public String str2() { return "Spring"; }
}

@Component
class FirstService {
    @Autowired
    @FirstQualifier
    private List<String> list;

    @PostConstruct
    public void init() {
        System.out.println(list);
    }
}
