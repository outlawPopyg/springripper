package com.example.springripper.parts.second;

import com.example.springripper.parts.second.annotations.Action;
import com.example.springripper.parts.second.annotations.AnyGenre;
import com.example.springripper.parts.second.annotations.Comedy;
import com.example.springripper.parts.second.annotations.Melodrama;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

public class Actors {
}

@Component @Comedy
class Jim implements Actor {}

@Component @Action
class Tobey implements Actor {}

@Component @Action @Comedy
class Stallone implements Actor {}

@Component @Melodrama
class Julia implements Actor {}

@Component @AnyGenre
class Norris implements Actor {}

@Component
class Katy implements Actor {}

@Service
class Film {
    @Autowired
    @Comedy
    @Action
    private List<Actor> actors;

    @PostConstruct
    public void init() {
        System.out.println(actors); // [Norris, Stallone]
    }
}