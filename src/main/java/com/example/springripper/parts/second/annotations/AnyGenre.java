package com.example.springripper.parts.second.annotations;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Comedy
@Action
@Melodrama
public @interface AnyGenre {
}
