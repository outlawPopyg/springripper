package com.example.springripper.parts.second;

import com.example.springripper.parts.second.annotations.SecondQualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class SecondConfig {

    @Bean
    @SecondQualifier
    public List<String> messages() {
        ArrayList<String> messages = new ArrayList<>();
        messages.add("Java");
        messages.add("Spark");
        return messages;
    }
}

@Component
class SecondService {
    @Autowired
    @SecondQualifier
    private List<String> list;

    @PostConstruct
    public void init() {
        System.out.println(list);
    }
}
