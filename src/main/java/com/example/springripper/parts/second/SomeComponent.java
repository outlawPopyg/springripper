package com.example.springripper.parts.second;

import org.springframework.stereotype.Component;

@Component
public class SomeComponent {
    @Override
    public String toString() {
        return "SomeComponent";
    }
}
