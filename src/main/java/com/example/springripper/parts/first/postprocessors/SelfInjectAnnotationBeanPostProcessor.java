package com.example.springripper.parts.first.postprocessors;

import com.example.springripper.parts.first.annotations.SelfInject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

//@Component
public class SelfInjectAnnotationBeanPostProcessor implements BeanPostProcessor, Ordered {
    private Map<String, Object> map = new HashMap<>();
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (beanName.equals("terminatorQuoter")) {
            int i = 0;
        }
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(SelfInject.class)) {
                map.put(beanName, bean);
                break;
            }
        }
        return bean;
    }

    // сюда уже приходит proxy-bean, сгенеренный в ProfilingBeanPostProcessor
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Object originalBean = map.get(beanName);
        if (originalBean != null) {
            Field[] fields = originalBean.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(SelfInject.class)) {
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, originalBean, bean); // инжектим в поле оригинального класса прокси
                }
            }
        }

        return bean;
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}
