package com.example.springripper.parts.first.screensaver;

import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class ColorFrame extends JFrame {
    @Autowired
    private Color color;

    public ColorFrame(Color color) {
        setSize(200,200);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void showOnRandomPlace() {
        Random random = new Random();
        setLocation(random.nextInt(1200), random.nextInt(300));
        getContentPane().setBackground(color);
        repaint();
    }
}
