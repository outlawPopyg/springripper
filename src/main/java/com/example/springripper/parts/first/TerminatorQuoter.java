package com.example.springripper.parts.first;

import com.example.springripper.parts.first.annotations.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Profiling
//@DeprecatedClass(newImpl = T1000.class)
public class TerminatorQuoter implements Quoter, SomeInterface {

    @SelfInject
    private Quoter terminatorQuoter;

    @InjectId
    public String id;

    public TerminatorQuoter() {
        System.out.println("Phase 1 " + this.getClass());
    }

    @PostConstruct
    public void init() {
        System.out.println("Phase 2 " + this.getClass());

        System.out.println(id);
    }

    @Override
    @PostProxy
    public void sayQuote() {
        System.out.println("Phase 3 " + this.getClass());
        System.out.println("I'll be back");
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void someMethod() {
        System.out.println("someMethod()");
    }
}
