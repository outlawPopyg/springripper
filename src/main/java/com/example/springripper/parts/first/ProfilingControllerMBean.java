package com.example.springripper.parts.first;

public interface ProfilingControllerMBean {
    void setEnabled(boolean flag);
}
