package com.example.springripper.parts.first.postprocessors;

import com.example.springripper.parts.first.annotations.InjectId;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.UUID;

// BeanPostProcessor отрабатывает перед тем, как бин попадет в контекст
public class InjectIdAnnotationBeanPostProcessor implements BeanPostProcessor {

    // BEFORE init() method
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        Field[] fields = beanClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(InjectId.class)) {
                field.setAccessible(true);
                int i = 9;
                ReflectionUtils.setField(field, bean, UUID.randomUUID().toString());
            }
        }

        return bean;
    }

    // AFTER init() method
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
