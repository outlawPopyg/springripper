package com.example.springripper.parts.first;

import org.springframework.stereotype.Service;

@Service
public class ProfilingController implements ProfilingControllerMBean {
    private boolean enabled = true;
    public boolean isEnabled() {
        return enabled;
    }
    @Override
    public void setEnabled(boolean flag) {
        this.enabled = flag;
    }
}
