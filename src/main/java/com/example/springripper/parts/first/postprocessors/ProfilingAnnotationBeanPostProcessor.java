package com.example.springripper.parts.first.postprocessors;

import com.example.springripper.parts.first.annotations.Profiling;
import com.example.springripper.parts.first.ProfilingController;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

//@Component
public class ProfilingAnnotationBeanPostProcessor implements BeanPostProcessor, Ordered {
    Map<String, Class<?>> classMap = new HashMap<>();
    private final ProfilingController profilingController;

    public ProfilingAnnotationBeanPostProcessor (ProfilingController profilingController) {
        try {
            MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            mBeanServer.registerMBean(profilingController, new ObjectName("profiling", "name", "controller"));
            this.profilingController = profilingController;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Profiling.class)) {
            classMap.put(beanName, beanClass);
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = classMap.get(beanName);
        if (beanClass != null) {
            Class<?> aClass = bean.getClass();
            return Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(),
                    ((proxy, method, args) -> {
                        if (profilingController.isEnabled()) {
                            System.out.println("[PROFILING...]");
                            Object retVal = method.invoke(bean, args);
                            return retVal;
                        } else {
                            return method.invoke(bean, args);
                        }
                    }));
        }

        return bean;

    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }
}




