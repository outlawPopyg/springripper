package com.example.springripper.parts.first.configuration;


import com.example.springripper.parts.first.listeners.PostProxyInvokerContextListener;
import com.example.springripper.parts.first.postprocessors.InjectIdAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Bean
    public InjectIdAnnotationBeanPostProcessor injectIdAnnotationBeanPostProcessor() {
        return new InjectIdAnnotationBeanPostProcessor();
    }

    @Bean
    public PostProxyInvokerContextListener postProxyInvokerContextListener() {
        return new PostProxyInvokerContextListener();
    }
}


